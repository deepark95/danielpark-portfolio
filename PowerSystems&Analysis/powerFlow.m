% Daniel (Seung Won) Park
% EE 454
% Final Project
% powerFlow Function

function RESULTS = powerFlow(L_I, inputPQ, SH, pu, n)

%% INPUTS
% inputPQ contains cell that has the list of buses and the PQ Matrix
Type_PQ = inputPQ{1,1};
PQ_Matrix = inputPQ{1,2};

% if power is not in pu --> convert the injections to pu
scale = 1;
base = 100;
if(pu==0)
    PQ_Matrix(:,2:5) = PQ_Matrix(:,2:5)/100;
end
   
%% Y-MATRIX

% Take in the first table to convert to Y-admittance Matrix
row = size(L_I, 1);

% go through matrix to see the number of nodes
% in the system
maxNodes = 0;
for a = 1: row
    for b = 1:2
        if(maxNodes< L_I(a,b))
            maxNodes = L_I(a,b);
        end
    end
end

% create new matrix based on number of nodes
Y_A = zeros(maxNodes, maxNodes);

% (col, row)
% look at each row
% i -> columns
% j -> rows 
for i=1:row
    R = L_I(i, 3); %resistance
    X = L_I(i, 4); %reactance
    Y = L_I(i, 5); %impedance from capacitor

    % Add into the diagonal of first node (includes Y/2)
    Y_A(L_I(i, 1),L_I(i, 1)) = Y_A(L_I(i, 1),L_I(i, 1))+ 1/(R+j*X) + j*Y;
    % Add into the diagonal of second node (includes Y/2)
    Y_A(L_I(i, 2),L_I(i, 2)) = Y_A(L_I(i, 2),L_I(i, 2))+ 1/(R+j*X) + j*Y;

    % Node1, Node2 (exclude Y/2)
    Y_A(L_I(i, 1),L_I(i, 2)) = Y_A(L_I(i, 1),L_I(i, 2)) - 1/(R+j*X);

    % Node2, Node1 (exclude Y/2)
    Y_A(L_I(i, 2),L_I(i, 1)) = Y_A(L_I(i, 2),L_I(i, 1)) - 1/(R+j*X);
end

% Add in the shunt elements into the Y-Matrix
% At the 'ith' node
if(SH~=0)
    for i = 1:size(SH,1)
        Y_A(SH(i, 1),SH(i, 1)) = Y_A(SH(i, 1),SH(i, 1)) + j*SH(i, 3);
    end
end
% Separate the real and imaginary parts of the Y-Matrix
B = imag(Y_A);
G = real(Y_A);


%% CREATING SYMS/VARIABLES
% keep count of how many variables (syms) needed
unk_V = sym(PQ_Matrix(:,6));
unk_O = sym([]);
jacobV = [];
jacobO = [];

% Create the necessary SYMS/Variables needed for Jacobian
for i = 1:size(PQ_Matrix,1)
    if(strcmp(Type_PQ(i),'PQ')) % need V and O
        unk_V(i) = sym(sprintf('V%d', i));
        unk_O = [unk_O;sym(sprintf('o%d', i))];
        jacobV = [jacobV,sym(sprintf('V%d',i))];
        jacobO = [jacobO,sym(sprintf('o%d',i))];
        
    end
    if(strcmp(Type_PQ(i),'PV')) % need O
        unk_O = [unk_O;sym(sprintf('o%d', i))];
        jacobO = [jacobO,sym(sprintf('o%d',i))];
        
        % if PV bus, add the necessary reactance
        Y_A(i,i)=Y_A(i,i)+1/(0.05*j);
    end
    
    if(strcmp(Type_PQ(i),'SL')) % need 
        unk_O = [unk_O;0];
        % if slack bus, add the necessary reactance
        Y_A(i,i)=Y_A(i,i)+1/(0.05*j);
    end
end

V = unk_V
O = unk_O

% Implicit and Explicit Equations
im_Q = [];
im_P = [];

ee_Store = [];

%% CREATE IMPLICIT EQUATIONS and EXPLICIT EQUATIONS
% At the "kth" node 
for k=1:size(PQ_Matrix,1) 
    % Keep storage of the summations
    Psum = 0;
    Qsum = 0;

    % Equations (Both Implicit and Explicit) from summations
    for i = 1: maxNodes
        Psto = V(k)*V(i)*(G(k,i)*cos(O(k)-O(i))+B(k,i)*sin(O(k)-O(i)));
        Psum = Psum+Psto;
        Qsto = V(k)*V(i)*(G(k,i)*sin(O(k)-O(i))-B(k,i)*cos(O(k)-O(i)));
        Qsum = Qsum+Qsto;
    end
    
    % If PQ Bus, store "P" and "Q" into implicit
    if(strcmp(Type_PQ(k),'PQ'))
        im_P = [im_P; (Psum-PQ_Matrix(k,2)+PQ_Matrix(k,4))];
        im_Q = [im_Q; (Qsum-PQ_Matrix(k,3)+PQ_Matrix(k,5))];
    end
    
    % If PV Bus, store "P" into implicit and "Q" into explicit
    if(strcmp(Type_PQ(k),'PV'))
        im_P = [im_P; (Psum-PQ_Matrix(k,2)+PQ_Matrix(k,4))];
        ee_Store = [ee_Store;Qsum];
    end
    
    % If SL Bus, store "P" and "Q" into explicit
    if(strcmp(Type_PQ(k),'SL'))
        ee_Store = [ee_Store;Psum;Qsum];
    end
end
im_Store = [im_P;im_Q];

%% JACOBIAN PROCESS
jacobVariables = transpose([jacobO,jacobV]);
J = jacobian(im_Store, jacobVariables);
ie = matlabFunction(im_Store);
ee = matlabFunction(ee_Store);

in = transpose([zeros(1,size(jacobO,2)),ones(1,size(jacobV,2))]);

% n = number of iterations
for i = 1:n
    i
    outJacob = double(subs(J, [jacobVariables], [in]))
    Iacob = inv(outJacob);
    
    % find the mismatch implicit equations
    initial = double(subs(im_Store,[jacobVariables],[in]));

    %% Apply Difference Via Deltas to Find New Points
    delta = -Iacob*initial
    in = in + delta;
    
end % End of the iteration

RESULTS = [in, delta];
end % END OF THE FUNCTION