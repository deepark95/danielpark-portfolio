# Multiple Power-flow Stability Project using MATLAB

# Author(s):
* Daniel Park

# Dates
Start: November 2015  
End:   December 2015

## Description:
The basis of this project is to implement an algorithm that numerically
solves implicit and explicit power flow (voltage/current) equations
with given initial conditions. A MATLAB script is written to do
so and is contained within the repository.

The following is a description of the purpose each MATLAB script within the
project directory:

* EE454_FinalProject_TestFile.m:
This script file sets up the inputs for the powerFlow.m.

* powerFlow.m:
The powerFlow.m is a function file that runs the numerical solving algorithm.

* EE454_FINAL_PROJECT.m:
This file combines both of the previous file together into one script file.