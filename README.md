## DANIEL PARK'S GIT PORTFOLIO
Contact: dswpark@uw.edu 
https://www.linkedin.com/in/daniel-park-860916ab/

## Education:
School: University of Washington  
Bachelor of Science: Electrical Engineering; GPA: 3.72  
Minor: Math

## Interests:
* Power Electronics and Circuit Design
* Software and Web Development
* Power Systems and Analysis
* Embedded Systems

## Side-Hobbies/Activites:
* Drumming
* Playing Guitar
* United Life Ministry Praise Team (Bassist)
* Swimming
* Weightlifting

## Repo Description:

This is a database of relevant reports and documents for all of the major
assignments and projects I have done so far. The following is a list of 
projects I have worked throughout the years within this repository. 
Refer to each directory for more information of the projects.

* (Echodyne): Automatic Drone Detection Graphical-User-Interface Development
* (MotorDrive): Brushless DC Motor Drive Circuit Design Project
* (PassiveWifi): Demo Implementation of the Passive Wifi Backscatter Project
* (PowerSystem&Analysis): Power Flow and Stability Project w/ MATLAB
* (Robo-TankEmbedded): Robo-Tank Embedded Systems Project
* (FlybackDC_Converter): DC/DC Flyback Converter
