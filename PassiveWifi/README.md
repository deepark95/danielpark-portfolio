# Demo Implementation of the Passive Wifi Project

# Author(s):
* Daniel Park
* Huibo Zhao

# Date
Start: March 2016  
End:   September 2016

## Description:
The purpose of this project is to create a demo implementation of the
Passive Wifi Project. Initially, the team had to research on the structure
of the 802.11b wifi system and its data packets. From there, we were to
study the packet structures for the different speeds within the 802.11b
spectrum and use that knowledge to create modulation algorithms to achieve that
with test input bits.