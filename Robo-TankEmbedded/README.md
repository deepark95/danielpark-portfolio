# Robo Tank Embedded Systems Final Project

# Author(s):
* Daniel Park
* Reilly Mulligan
* Samuel Johnson

# Dates
Start: October 2016  
End:   December 2016

## Description:
With given two motors, a tank chassis, and a Beaglebone microcontroller, our
project was to develop a moving tank that can automatically self-sufficently 
drive and can be controlled via android phone application.

Refer to the report for more information. Relevant code are not in this repo.
Please contact one of the authors for code disclosure and sharing.