# Automatic Drone Detection, Graphical-User-Interface (GUI) Project

# Authors:
* Daniel Park
* Skyler Martens
* Bill (Jianfeng) Huang
* Huy Nguyen

Project sponsored by Echodyne (based in Bellevue, WA)

# Dates
Start: January 2017  
End:   June 2017

## Description:
The basis of this project is to create an GUI for Echodyne's developing,
state-of-the-art Metamaterial Electronically Scanning Array (MESA) Radar.
This means taking raw data from the radar, processing such data, and display
such information for the user to understand. Refer to the report for more
detailed information regarding the project.

Please note, the GIT repo project doesn't contain any of the developed code 
as the code is only meant for Echodyne only and is classified. Only the report
is contained, which explains the research done and the top-level descriptions 
of the interface itself for any reader to understand. Please contact Echodyne
for any access to any of the code relevant in this project.