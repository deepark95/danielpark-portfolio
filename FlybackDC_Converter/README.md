# DC/DC Flyback Converter

# Author(s):
* Daniel Park
* Nasir Elmi
* Ki Hei Chan

# Dates
Start: November 2015  
End:   December 2015

## Description:
The basis of this project was to construct a DC/DC Flyback Converter.
In a open loop version of the circuit, the converter is able to perform
buck and boost behavior from 10V input to output a 5-15 V DC.
Additionally, the converter is able to output 15 V DC in a closed loop
circuit, which is more stable in varying loads and input voltage magnitude.