# Brushless DC Motor Drive and Controller Project

# Author(s):
* Jacob Staudt
* David Chon
* Josh Park
* Daniel Park

# Dates
Start: January 2016  
End:   March 2016

## Description:
The basis of this project is to create an driver and controller for a
motor, which takes in three phase inputs and had three hall sensor
outputs. With this motor, the team was able to create a driver with
multiple features, high-current/voltage protection, etc. See the report
for more details and specifications of our product.